﻿using EmployeesManagement.Models;
using EmployeesManagement.Persistence;
using EmployeesManagement.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace EmployeesManagement.UnitTest.Services
{
    public class AuthenticationTests
    {
        private readonly AuthenticationManager _auth;

        public AuthenticationTests()
        {
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase(databaseName: "appDb")
                .Options;

            var context = new AppDbContext(options);

            var user = new User
            {
                Username = "user",
                Password = "pass"
            };
            context.Users.Add(user);

            _auth = new AuthenticationManager(context);
        }

        [Fact]
        public void Login_CredentialsAreCorrect_ReturnTrue()
        {
            var correctUser = new User
            {
                Username = "user",
                Password = "pass"
            };

            var result = _auth.Login(correctUser);

            Assert.True(result);
        }

        [Fact]
        public void Login_LoginDataIsIncorrect_ReturnFalse()
        {
            var wrongUser = new User
            {
                Username = "WrongUser",
                Password = "WrongPass"
            };

            var result = _auth.Login(wrongUser);

            Assert.False(result);
        }

        [Fact]
        public void Register_UserAdded_ReturnTrue()
        {
            var newUser = new User
            {
                Username = "NewUser",
                Password = "pass"
            };

            var result = _auth.Register(newUser);

            Assert.True(result);
        }

        [Fact]
        public void Register_UserNotAdded_ReturnFalse()
        {
            var existingUser = new User
            {
                // "user" already exists
                Username = "user",
                Password = "pass2"
            };

            var result = _auth.Register(existingUser);

            Assert.False(result);
        }
    }
}

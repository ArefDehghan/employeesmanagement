﻿using EmployeesManagement.Models;
using EmployeesManagement.Persistence;
using System;
using System.Linq;

namespace EmployeesManagement.Services
{
    public class CommandParser
    {
        private readonly AppDbContext _context;

        private readonly AuthenticationManager _authenticationManager;

        public CommandParser(AppDbContext context, AuthenticationManager authenticationManager)
        {
            _context = context;
            _authenticationManager = authenticationManager;
        }

        public void Authenticate()
        {
            Console.Clear();

            while (true)
            {
                Console.WriteLine("Commands:\n\t1. Register\n\t2. Login\n\t3. Exit\n");
                Console.Write("Home> ");
                var command = Console.ReadLine();
                Console.Clear();

                switch (command.ToLower().Trim())
                {
                    case "1":
                    case "register":
                        {
                            var newUser = new User();

                            Console.Write("Username: ");
                            var username = Console.ReadLine();

                            Console.Write("Password: ");
                            var password = Console.ReadLine();

                            Console.Write("Re-Enter Password: ");
                            var reEnterPassword = Console.ReadLine();

                            if (password != reEnterPassword)
                            {
                                Console.WriteLine("Passwords doesn't match.");
                                break;
                            }

                            newUser.Username = username;
                            newUser.Password = password;

                            if (!_authenticationManager.Register(newUser))
                            {
                                Console.WriteLine("Username already exists.");
                                break;
                            }

                            Console.Clear();
                            Management(newUser.Username);
                            break;
                        }
                    
                    case "2":
                    case "login":
                        {
                            var user = new User();

                            Console.Write("Username: ");
                            user.Username = Console.ReadLine();

                            Console.Write("Password: ");
                            user.Password = Console.ReadLine();

                            if (!_authenticationManager.Login(user))
                            {
                                Console.WriteLine("Username or Password is incorrect.");
                                break;
                            }

                            Console.Clear();
                            Management(user.Username);
                            break;
                        }

                    case "3":
                    case "exit":
                        Environment.Exit(0);
                        break;

                    default:
                        Console.WriteLine("Error: Command Not Found.");
                        break;
                }

            }
        }

        public void Management(string username)
        {
            while (true)
            {
                Console.WriteLine("Commands:\n\t1. List\n\t2. New\n\t3. Edit [ID]\n\t4. Delete [ID]\n\t5. Logout\n\t6. Exit\n");
                Console.Write($"{username}> ");
                var command = Console.ReadLine().Split(' ');
                Console.Clear();

                switch (command[0].ToLower().Trim())
                {
                    case "1":
                    case "list":
                        {
                            var employees = _context.Employees.ToList();

                            if (employees.Count == 0)
                            {
                                Console.WriteLine("Error: No employees exist.");
                                break;
                            }

                            foreach (var employee in employees)
                                Console.WriteLine(employee.ToString());

                            break;
                        }

                    case "2":
                    case "new":
                        {
                            var employee = new Employee();
                            ReadEmployee(employee);

                            if (!IsEmployeeValid(employee))
                            {
                                Console.WriteLine("Error: Enter all fields.");
                                break;
                            }

                            if (IsEmployeeExist(employee.NationalCode))
                            {
                                Console.WriteLine("Error: An employee already exist with this national code.");
                                break;
                            }

                            _context.Employees.Add(employee);
                            _context.SaveChanges();

                            Console.Clear();
                            break;
                        }

                    case "3":
                    case "edit":
                        {
                            if (command.Length != 2)
                            {
                                Console.WriteLine("Error: Command Not Found.");
                                break;
                            }

                            if (!int.TryParse(command[1], out int employeeId))
                            {
                                Console.WriteLine("Error: You must enter employee Id as an argument.");
                                break;
                            }

                            var employee = _context.Employees.SingleOrDefault(e => e.Id == employeeId);

                            if (employee == null)
                            {
                                Console.WriteLine($"Error: No employee found with this Id: {employeeId}.");
                                break;
                            }

                            ReadEmployee(employee);

                            if (!IsEmployeeValid(employee))
                            {
                                Console.WriteLine("Error: Enter all fields.");
                                break;
                            }

                            if (IsEmployeeExist(employee.NationalCode))
                            {
                                Console.WriteLine("Error: An employee already exist with this national code.");
                                break;
                            }

                            _context.SaveChanges();
                            Console.Clear();
                            break;
                        }

                    case "4":
                    case "delete":
                        {
                            if (command.Length != 2)
                            {
                                Console.WriteLine("Error: Command Not Found.");
                                break;
                            }

                            if (!int.TryParse(command[1], out int employeeId))
                            {
                                Console.WriteLine("Error: You must enter employee Id as an argument.");
                                break;
                            }

                            if (!IsEmployeeExist(employeeId))
                            {
                                Console.WriteLine($"Error: No employee found with this Id: {employeeId}.");
                                break;
                            }
                            
                            var employee = _context.Employees.SingleOrDefault(e => e.Id == employeeId);

                            Console.WriteLine($"\nWarning: You are about to delete an employee, are tou sure? [Y/N]");
                            var key = Console.ReadKey();
                            Console.Clear();

                            if (key.KeyChar == 'y')
                            {
                                _context.Remove(employee);
                                _context.SaveChanges();
                            }

                            break;
                        }

                    case "5":
                    case "logout":
                        Authenticate();
                        break;

                    case "6":
                    case "exit":
                        Environment.Exit(0);
                        break;

                    default:
                        Console.WriteLine("Error: Command Not Found.");
                        break;
                }
            }
        }

        private bool IsEmployeeValid(Employee employee)
        {
            if (string.IsNullOrWhiteSpace(employee.FirstName) || string.IsNullOrWhiteSpace(employee.LastName) || string.IsNullOrWhiteSpace(employee.MobileNumber) || string.IsNullOrWhiteSpace(employee.Email) || string.IsNullOrWhiteSpace(employee.NationalCode))
                return false;

            return true;
        }

        private bool IsEmployeeExist(string employeeNationalCode)
        {
            var existingEmployee = _context.Employees.SingleOrDefault(e => e.NationalCode == employeeNationalCode);

            return existingEmployee != null;
        }

        private bool IsEmployeeExist(int employeeId)
        {
            var existingEmployee = _context.Employees.SingleOrDefault(e => e.Id == employeeId);

            return existingEmployee != null;
        }

        private void ReadEmployee(Employee employee)
        {
            Console.Write("FirstName: ");
            employee.FirstName = Console.ReadLine();

            Console.Write("LastName: ");
            employee.LastName = Console.ReadLine();

            Console.Write("MobileNumber: ");
            employee.MobileNumber = Console.ReadLine();

            Console.Write("Email: ");
            employee.Email = Console.ReadLine();

            Console.Write("NationalCode: ");
            employee.NationalCode = Console.ReadLine();
        }

    }
}

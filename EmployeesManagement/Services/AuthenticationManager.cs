﻿using EmployeesManagement.Models;
using EmployeesManagement.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeesManagement.Services
{
    /// <summary>
    /// Handles user's login and registration.
    /// </summary>
    public class AuthenticationManager
    {
        private readonly AppDbContext _context;

        public AuthenticationManager(AppDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Validates usesr credentials.
        /// </summary>
        /// <param name="user"></param>
        /// <returns>True if credentials are correct.</returns>
        public bool Login(User user)
        {
            var userInDb = _context.Users.SingleOrDefault(u => u.Username == user.Username &&
                                                                  u.Password == user.Password);

            return userInDb != null;
        }

        /// <summary>
        /// Registers user.
        /// </summary>
        /// <param name="user"></param>
        /// <returns>True if user registered successfully.</returns>
        public bool Register(User user)
        {
            var userInDb = _context.Users.SingleOrDefault(u => u.Username == user.Username);

            if (userInDb != null)
                return false;

            _context.Users.Add(user);
            return _context.SaveChanges() > 0;
        }
    }
}

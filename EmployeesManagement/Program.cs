﻿using EmployeesManagement.Models;
using EmployeesManagement.Persistence;
using EmployeesManagement.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace EmployeesManagement
{
    class Program
    {
        static void Main(string[] args)
        {
            var services = new ServiceCollection()
                .AddScoped<CommandParser>()
                .AddScoped<AuthenticationManager>()
                .AddDbContext<AppDbContext>(options =>
                    options.UseInMemoryDatabase(databaseName: "AppDb"))
                .BuildServiceProvider();

            var commandParser = services.GetService<CommandParser>();
            commandParser.Authenticate();
        }
    }
}

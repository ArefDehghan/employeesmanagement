﻿using System.ComponentModel.DataAnnotations;

namespace EmployeesManagement.Models
{
    public class Employee
    {
        public int Id { get; set; }

        [Required]
        public string NationalCode { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string MobileNumber { get; set; }

        [Required]
        public string Email { get; set; }

        public override string ToString() => 
            $"Id: {Id} " +
            $"First Name: {FirstName} " +
            $"Last Name: {LastName} " +
            $"National Code: {NationalCode} " +
            $"Mobile Number: {MobileNumber} " +
            $"Email: {Email}";
    }
}
